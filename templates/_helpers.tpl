{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "trinity-wotlk.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "trinity-wotlk.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "trinity-wotlk.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "trinity-wotlk.labels" -}}
helm.sh/chart: {{ include "trinity-wotlk.chart" . }}
{{ include "trinity-wotlk.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "trinity-wotlk.selectorLabels" -}}
app.kubernetes.io/name: {{ include "trinity-wotlk.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "trinity-wotlk.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "trinity-wotlk.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Secrets
*/}}
{{- define "trinity-wotlk.mysql-secret" -}}
{{- if .Values.mysqlSecret -}}
{{- .Values.mysqlSecret }}
{{- else }}
{{- printf "%s-mysql-secret" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}
{{- end }}

{{- define "trinity-wotlk.mysql-pvc" -}}
{{- if .Values.mysql.persistence.claimName }}
{{- .Values.mysql.persistence.claimName }}
{{- else }}
{{- printf "%s-mysql-pvc" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}
{{- end }}

{{- define "trinity-wotlk.init-db-scripts" -}}
{{- printf "%s-init-db-scripts" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}

{{- define "trinity-wotlk.apply-custom-sql" -}}
{{- printf "%s-apply-custom-sql" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}

{{- define "trinity-wotlk.init-realmlist" -}}
{{- printf "%s-init-realmlist" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}

{{- define "trinity-wotlk.worldserver-conf" -}}
{{- printf "%s-worldserver-conf" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}

{{- define "trinity-wotlk.authserver-conf" -}}
{{- printf "%s-authserver-conf" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}

{{- define "trinity-wotlk.db-conn-secret" -}}
{{- if hasKey .Values.database_connection_settings "secretName" -}}
{{- .Values.database_connection_settings.secretName }}
{{- else }}
{{- printf "%s-db-conn-settings" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}
{{- end }}

{{- define "trinity-wotlk.mapdata-pvc" -}}
{{- if hasKey .Values.external_resources.mapdata "existingClaimName" -}}
{{- .Values.external_resources.mapdata.existingClaimName }}
{{- else }}
{{- printf "%s-mapdata-pvc" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}
{{- end }}

{{- define "trinity-wotlk.world-db-pvc" -}}
{{- if hasKey .Values.external_resources.world_database "existingClaimName" -}}
{{- .Values.external_resources.world_database.existingClaimName }}
{{- else }}
{{- printf "%s-world-db-pvc" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}
{{- end }}

{{- define "trinity-wotlk.mapdata-pv" -}}
{{- printf "%s-mapdata-pv" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}

{{- define "trinity-wotlk.mysql-service-name" -}}
{{- printf "%s-database" (include "trinity-wotlk.fullname" .) | trimSuffix "-" }}
{{- end }}
