use world;
DELETE FROM creature_template WHERE entry=601900;
DELETE FROM creature WHERE id=601900;
INSERT INTO creature_template (entry,difficulty_entry_1,difficulty_entry_2,difficulty_entry_3,KillCredit1,KillCredit2,modelid1,modelid2,modelid3,modelid4,name,gossip_menu_id,minlevel,maxlevel,exp,faction,npcflag,speed_walk,speed_run,scale,rank,dmgschool,BaseAttackTime,RangeAttackTime,BaseVariance,RangeVariance,unit_class,unit_flags,unit_flags2,dynamicflags,family,type,type_flags,lootid,pickpocketloot,skinloot,PetSpellDataId,VehicleId,mingold,maxgold,MovementType,HoverHeight,HealthModifier,ManaModifier,ArmorModifier,DamageModifier,ExperienceModifier,RacialLeader,movementId,RegenHealth,mechanic_immune_mask,spell_school_immune_mask,flags_extra,ScriptName,VerifiedBuild,AIName) VALUES
(601900,0,0,0,0,0,27510,0,0,0,"Execute Training Dummy",0,83,83,2,7,0,1,1,1,3,0,2000,2000,1,1,1,0,2048,0,0,9,4,0,0,0,0,0,0,0,0,1,1721.76,1,1,35,1,0,0,0,32,0,1074004032,"execute_target_dummy",12340, "execute_target_dummyAI");

INSERT INTO creature (guid,id,map,zoneId,areaId,spawnMask,phaseMask,modelid,equipment_id,position_x,position_y,position_z,orientation,spawntimesecs,wander_distance,currentwaypoint,curhealth,curmana,MovementType,npcflag,unit_flags,dynamicflags,VerifiedBuild) VALUES
(2000362,601900,1,0,0,1,1,0,0,16199.9,16273.8,10.7436,6.27815,300,0,0,24009944,0,0,0,0,0,0);
