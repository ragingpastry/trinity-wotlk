import os
import mysql.connector
import sys

with open('data_pairs.txt', 'r') as data:
    trainer_spell_list = []
    for line in data.readlines():
        trainer_spell_list.append(
            [item.strip() for item in line.split(',')]
        )

    for spell_pair in trainer_spell_list:
        # lookup spell in DB and grab and entry that does NOT have an id of the trainerID
        # Then create a new insert with the entry we just looked up replacing the trainer ID with the first item of the list
        # output this as a value array (int, int, int, int, int),
        old_trainer_id = spell_pair[0]
        spell_id = spell_pair[1]

        try:
            mysql_host = os.environ['MYSQL_DATABASE_HOST']
            mysql_user = os.environ['MYSQL_TRINITY_USERNAME']
            mysql_password = os.environ['MYSQL_TRINITY_PASSWORD']
        except KeyError as err:
            print(f"Required env variable not found! You must set MYSQL_DATABASE_HOST, MYSQL_TRINITY_USERNAME, and MYSQL_TRINITY_PASSWORD. {err}")
        # Check if username already exists
        try:
            worlddb = mysql.connector.connect(
                host = mysql_host,
                user = mysql_user,
                port=3306,
                password = mysql_password,
                database = "world"
            )
            cursor = worlddb.cursor(buffered=True)
            cursor.execute(f"SELECT * FROM trainer_spell where spellid={spell_id} and trainerid!={old_trainer_id} LIMIT 1")
            try:
                old_trainer = list(cursor.fetchone())
                old_trainer[0] = old_trainer_id
                new_trainer_values = [str(value) for value in old_trainer]
                print(f"({','.join(new_trainer_values)}),")
            except (AttributeError,TypeError) as e:
                pass
        except mysql.connector.Error as err:
            print("Something went wrong: {}".format(err))
        finally:

            cursor.close()
